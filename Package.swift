// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "TradingServer",
    platforms: [
        .macOS(.v10_12),
    ],
    products: [
        .library(name: "TradingServer", targets: ["App"]),
    ],
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),

        // 🔵 Swift ORM (queries, models, relations, etc) built on SQLite 3.
        .package(url: "https://github.com/vapor/fluent-postgresql.git", from: "1.0.0"),
        .package(url: "../Core/TradingCore", .branch("master")),
        .package(url: "https://github.com/SwiftyBeaver/SwiftyBeaver.git", .exact("1.8.4")),
        .package(url: "https://github.com/vapor-community/swiftybeaver-provider.git", from: "3.0.0")
    ],
    targets: [
        .target(
            name: "App", 
            dependencies: [
                "FluentPostgreSQL", 
                "Vapor", 
                "TradingCore", 
                "SwiftyBeaverProvider"
            ]
        ),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

