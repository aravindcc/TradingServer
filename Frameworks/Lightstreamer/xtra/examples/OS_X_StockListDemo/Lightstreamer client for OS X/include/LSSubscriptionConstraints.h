//
//  LSSubscriptionConstraints.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>

/**
 * The LSSubscriptionConstraints class collects the constraints to be applied on the data flow from a Subscription.
 */
@interface LSSubscriptionConstraints : NSObject <NSCopying> {}


#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSSubscriptionConstraints object with the specified max frequency in updates/sec.
 * <BR/><B>Edition Note:</B> a further global frequency limit is also imposed by the Server, if it is running in Presto, Allegro or 
 * Moderato edition; this specific limit also applies to RAW mode and to unfiltered dispatching.
 *
 * @return The LSSubscriptionConstraints object.
 */
+ (LSSubscriptionConstraints *) constraintsWithMaxFrequency:(double)maxFrequency;


#pragma mark -
#pragma mark Properties

/**
 * Frequency constraint in updates/sec.
 * <BR/>When changing max frequency, remember that the server may refuse the change if the subscription has been specified as unfiltered.
 * <BR/><B>Edition Note:</B> a further global frequency limit is also imposed by the Server, if it is running in Presto, Allegro or 
 * Moderato edition; this specific limit also applies to RAW mode and to unfiltered dispatching.
 */
@property (nonatomic, assign) double maxFrequency;


@end
