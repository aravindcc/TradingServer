//
//  LSUpdateInfo.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>


/**
 * The LSUpdateInfo class provides information about an update for a subscribed item.
 * The new and previous values for the subscribed fields are reported.
 * <BR/>NOTE: If the table subscription configuration enables the "COMMAND logic", then 
 * the old field values are referred to the same key.
 * <BR/>Both names and positional information can be used to identify the specific fields, unless an LSTableInfo was used
 * to describe the related table: in that case, only positional information can be used.
 */
@interface LSUpdateInfo : NSObject {}


#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSUpdateInfo object with the specified parameters.
 * 
 * @param itemPosition The 1-based position of the item in the Group of items in the related table.
 * @param itemName The item name, or nil if an LSTableInfo was used to describe the related table.
 * @param fieldNames An array of strings containing the names of fields of the Schema used to subscribe the table, 
 * or nil if an LSTableInfo was used to describe the related table.
 * @param oldValues An array of strings containing the previous values of fields of the Schema.
 * @param newValues An array of strings containing the current values of fields of the Schema.
 * @param isSnapshot Tells if the update is part of the initial snapshot.
 *
 * @return The LSUpdateInfo object.
 */
+ (LSUpdateInfo *) updateInfoForItemPosition:(int)itemPosition itemName:(NSString *)itemName fieldNames:(NSArray *)fieldNames oldValues:(NSArray *)oldValues newValues:(NSArray *)newValues isSnapshot:(BOOL)isSnapshot;

/**
 * Initializes an LSUpdateInfo object with the specified parameters.
 * 
 * @param itemPosition The 1-based position of the item in the Group of items in the related table.
 * @param itemName The item name, or nil if an LSTableInfo was used to describe the related table.
 * @param fieldNames An array of strings containing the names of fields of the Schema used to subscribe the table,
 * or nil if an LSTableInfo was used to describe the related table.
 * @param oldValues An array of strings containing the previous values of fields of the Schema.
 * @param newValues An array of strings containing the current values of fields of the Schema.
 * @param isSnapshot Tells if the update is part of the initial snapshot.
 *
 * @return The LSUpdateInfo object.
 */
- (id) initWithItemPosition:(int)itemPosition itemName:(NSString *)itemName fieldNames:(NSArray *)fieldNames oldValues:(NSArray *)oldValues newValues:(NSArray *)newValues isSnapshot:(BOOL)isSnapshot;

#pragma mark -
#pragma mark Public methods

/**
 * Tells if the value for a field has changed after the reception of this update. For the first update for the item, it always returns YES.
 * 
 * @param fieldPosition The 1-based field position within the Schema of fields in the related table.
 *
 * @return YES if the updated value is different than the previous one.
 *
 * @throws NSInvalidArgumentException Thrown in case the specified field position does not represent a subscribed field.
 */
- (BOOL) isChangedValueOfFieldPosition:(int)fieldPosition;

/**
 * Tells if the value for a field has changed after the reception of this update. For the first update for the item, it always returns YES.
 * 
 * @param fieldName A field name.
 *
 * @return YES if the updated value is different than the previous one.
 *
 * @throws NSInvalidArgumentException Thrown in case the specified field name does not represent a subscribed field.
 * @throws LSException Thrown in case an LSTableInfo was used to describe the related table.
 */
- (BOOL) isChangedValueOfFieldName:(NSString *)fieldName;

/**
 * Gets the value for a field as it is after the reception of this update.
 * 
 * @param fieldPosition The 1-based field position within the Schema of fields in the related table.
 *
 * @return The field value (nil is legal value too).
 *
 * @throws NSInvalidArgumentException Thrown in case the specified field position does not represent a subscribed field.
 */
- (NSString *) currentValueOfFieldPosition:(int)fieldPosition;

/**
 * Gets the value for a field as it is after the reception of this update.
 * 
 * @param fieldName A field name.
 *
 * @return The field value (nil is legal value too).
 *
 * @throws NSInvalidArgumentException Thrown in case the specified field name does not represent a subscribed field.
 * @throws LSException Thrown in case an LSTableInfo was used to describe the related table.
 */
- (NSString *) currentValueOfFieldName:(NSString *)fieldName;

/**
 * Gets the value for a field as it was before the reception of this update.
 * 
 * @param fieldPosition The 1-based field position within the Schema of fields in the related table.
 *
 * @return The previous field value (nil is a legal value too).
 *
 * @throws NSInvalidArgumentException Thrown in case the specified field position does not represent a subscribed field.
 */
- (NSString *) previousValueOfFieldPosition:(int)fieldPosition;

/**
 * Gets the value for a field as it was before the reception of this update.
 * 
 * @param fieldName A field name.
 *
 * @return The previous field value (nil is a legal value too).
 *
 * @throws NSInvalidArgumentException Thrown in case the specified field name does not represent a subscribed field.
 * @throws LSException Thrown in case an LSTableInfo was used to describe the related table.
 */
- (NSString *) previousValueOfFieldName:(NSString *)fieldName;


#pragma mark -
#pragma mark Properties

/**
 * The 1-based position of the item in the Group of items in the related table.
 */
@property (nonatomic, readonly) int itemPosition;

/**
 * The item name, or nil if an LSTableInfo was used to describe the related table.
 */
@property (nonatomic, readonly) NSString *itemName;

/**
 * The number of fields of the Schema used to subscribe the table. In case an LSTableInfo was used to describe the related table,
 * this number might not be known at subscription time.
 */
@property (nonatomic, readonly) int numberOfFields;

/**
 * Tells if the update is part of the initial snapshot.
 */
@property (nonatomic, readonly) BOOL isSnapshot;


@end
