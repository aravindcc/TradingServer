//
//  LSPushConnectionException.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>
#import "LSException.h"

/**
 * The LSPushConnectionException class incapsulates exceptions thrown due to connectivy problems with the Server.
 */
@interface LSPushConnectionException : LSException
{
}

#pragma mark -
#pragma mark Initialization

/**
 * Creates and raises an LSPushConnectionException object with specified parameters.
 * 
 * @param reason Reason of the exception.
 *
 * @return The LSPushConnectionException object.
 */
+ (void)raiseWithReason:(NSString *)reason, ...;

/**
 * Initializes an LSPushConnectionException object with specified parameters.
 * 
 * @param reason Reason of the exception.
 *
 * @return The LSPushConnectionException object.
 */
- (id)initWithReason:(NSString *)reason, ...;

/**
 * Initializes an LSPushConnectionException object with specified parameters.
 * 
 * @param reason Reason of the exception.
 * @param arguments Variable argument list of parameters.
 *
 * @return The LSPushConnectionException object.
 */
- (id)initWithReason:(NSString *)reason arguments:(va_list)arguments;

@end
