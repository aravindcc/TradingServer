//
//  LSSubscribedTableKey.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>

/**
 * The LSSubscribedTableKey class contains the key value to be used to unsubscribe from tables.
 */
@interface LSSubscribedTableKey : NSObject <NSCopying>
{
}

#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSSubscribedTableKey object with the specified parameters.
 *
 * @param keyValue The key value of the table.
 *
 * @return The LSSubscribedTableKey object.
 */
+ (LSSubscribedTableKey *)tableKeyWithKeyValue:(int)keyValue;

/**
 * Initializes an LSSubscribedTableKey object with the specified parameters.
 *
 * @param keyValue The key value of the table.
 *
 * @return The LSSubscribedTableKey object.
 */
- (id)initWithKeyValue:(int)keyValue;

#pragma mark -
#pragma mark Properties

/**
 * The key value to be used to unsubscribe from the table.
 */
@property(nonatomic, readonly) int keyValue;

@end
