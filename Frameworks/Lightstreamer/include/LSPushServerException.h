//
//  LSPushServerException.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>
#import "LSException.h"

/**
 * The LSPushServerException class incapsulates exceptions thrown due to a specific error returned by the Server.
 */
@interface LSPushServerException : LSException
{
}

#pragma mark -
#pragma mark Initialization

/**
 * Creates and raises an LSPushServerException object with specified parameters.
 * 
 * @param errorCode The specific error code returned by the Server.
 * @param reason Reason of the exception.
 *
 * @return The LSPushServerException object.
 */
+ (void)raiseWithCode:(int)errorCode reason:(NSString *)reason, ...;

/**
 * Initializes an LSPushServerException object with specified parameters.
 * 
 * @param errorCode The specific error code returned by the Server.
 * @param reason Reason of the exception.
 *
 * @return The LSPushServerException object.
 */
- (id)initWithCode:(int)errorCode reason:(NSString *)reason, ...;

/**
 * Initializes an LSPushServerException object with specified parameters.
 * 
 * @param errorCode The specific error code returned by the Server.
 * @param reason Reason of the exception.
 * @param arguments Variable argument list of parameters.
 *
 * @return The LSPushServerException object.
 */
- (id)initWithCode:(int)errorCode reason:(NSString *)reason arguments:(va_list)arguments;

#pragma mark -
#pragma mark Properties

/**
 * The specific error code returned by the Server.
 */
@property(nonatomic, readonly) int errorCode;

@end
