/*
 *  LightstreamerClient.h
 *  Lightstreamer client for iOS
 *
 */

#import "LSConnectionStatus.h"
#import "LSClient.h"
#import "LSLog.h"
#import "LSMode.h"
#import "LSConnectionInfo.h"
#import "LSConnectionConstraints.h"
#import "LSSubscriptionConstraints.h"
#import "LSConnectionDelegate.h"
#import "LSException.h"
#import "LSPushConnectionException.h"
#import "LSPushServerException.h"
#import "LSPushUpdateException.h"
#import "LSUpdateInfo.h"
#import "LSTableInfo.h"
#import "LSExtendedTableInfo.h"
#import "LSTableDelegate.h"
#import "LSSubscribedTableKey.h"
#import "LSMessageInfo.h"
#import "LSMessageDelegate.h"

/**
 * @mainpage Getting started with the iOS and OS X Client libraries
 * 
 * The iOS and OS X Client Libraries have been developed using the Java SE library as a starting point.
 * Obtaining a connection and subscribing to a table is a simple and straightforward procedure:
 * <UL>
 *   <LI>Create an instance of LSClient</LI>
 *   <LI>Define an LSConnectionInfo object and open the connection</LI>
 *   <LI>When connected, define an LSTableInfo or LSExtendedTableInfo object and subscribe the table</LI>
 *   <LI>Start receiving the updates</LI>
 * </UL>
 *
 * @section create_lsclient Creating an instance of LSClient
 * <P>To create an instance of LSClient simply alloc- and init-it, or use the factory method:</P>
 * <BR/><BR/><CODE>LSClient *client= [[LSClient alloc] init];</CODE>
 * <P>Or:</P>
 * <BR/><BR/><CODE>LSClient *client= [LSClient client];</CODE>
 *
 * @section open_con Open the connection
 * <P>To open the connection you must first define the connection specifications using an LSConnectionInfo object, 
 * for example:</P>
 * <BR/><BR/><CODE>LSConnectionInfo *connectionInfo= [LSConnectionInfo connectionInfoWithPushServerURL:@@"http://push.lightstreamer.com" pushServerControlURL:nil user:nil password:nil adapter:@@"DEMO"];</CODE>
 *
 * <P>Done this, use the LSConnectionInfo object with the LSClient:</P>
 * <BR/><BR/><CODE>[client openConnectionWithInfo:connectionInfo delegate:self];</CODE>
 *
 * <P>Please note that this call is <B>blocking</B>: it will not return until a connection has been established.
 * Since this may take long, you may want to run it from a background thread, to avoid freezing the UI.</P>
 *
 * @section checking_con Listening for connection events
 * <P>Your designated delegate will then receive an event when the connection actually starts:</P>
 * <BR/><BR/><CODE>- (void) clientConnection:(LSClient *)client didStartSessionWithPolling:(BOOL)polling {<BR/>&nbsp;&nbsp;&nbsp;&nbsp;...<BR/>}</CODE>
 *
 * <P>Once this happens, you can start define an LSTableInfo object that describes the table you want to subscribe:</P>
 * <BR/><BR/><CODE>LSTableInfo *tableInfo= [LSTableInfo tableInfoWithGroup:@@"item1 item2 item3" mode:LSModeMerge schema:@@"stock_name last_price min max" dataAdapter:@@"QUOTE_ADAPTER" snapshot:YES];</CODE>
 * <P>And then subscribe it using the LSClient:</P>
 * <BR/><BR/><CODE>LSSubscribedTableKey *tableKey= [[_client subscribeTableWithExtendedInfo:tableInfo delegate:self useCommandLogic:NO] retain];</CODE>
 * <P>Remember to retain the <CODE>tableKey</CODE> to later unsubscribe it.</P>
 *
 * @section listen_table Listening for table events
 * <P>Your designated table delegate will then start to receive table updates as they are sent from the Server:</P>
 * <BR/><BR/><CODE>- (void) table:(LSSubscribedTableKey *)tableKey itemPosition:(int)itemPosition itemName:(NSString *)itemName didUpdateWithInfo:(LSUpdateInfo *)updateInfo {<BR/>&nbsp;&nbsp;&nbsp;&nbsp;...<BR/>}</CODE>
 * <P>Congratulations! Your subscription with your Lightstreamer Server is now set up!</P>
 *
 * @section close_con Close the connection
 * <P>Remember to always close the connection before releasing LSClient (it will not be released, otherwise):</P>
 * <BR/><BR/><CODE>[client closeConnection];</CODE>
 *
 * @section reconnection Automatic reconnections
 * <P>The library has an advanced logic to automatically reconnect to the server in case of connection drops, timeouts, etc.
 * The network availability is constantly monitored to attempt a reconnection only when it can be successful, and at
 * the same time to preserve the device battery. At reconnection, the library will also automatically resubmit any subscription 
 * that was active when the connection dropped. This logic is active by default, and will stop trying only in presence of a
 * clientConnection:didEndWithCause: event.</P>
 * <P>In view of this, avoid to manually manage the reconnection unless you have a strong reason to do so.
 * If you really need to, close the connection during appropriate events to block the library's reconnection logic,
 * e.g.:</P>
 * <BR/><BR/><CODE>- (void) clientConnection:(LSClient *)client didReceiveServerFailure:(LSPushServerException *)failure {<BR/>&nbsp;&nbsp;&nbsp;&nbsp;[client closeConnection];<BR/>}</CODE>
 * <P>or:</P>
 * <BR/><BR/><CODE>- (void) clientConnection:(LSClient *)client didReceiveConnectionFailure:(LSPushConnectionException *)failure {<BR/>&nbsp;&nbsp;&nbsp;&nbsp;[client closeConnection];<BR/>}</CODE>
 *
 * @section more_clients Using more than one LSClient
 * <P>The library allows the use of more than one LSClient instance at the same time. However, consider that more clients means more threads, bigger memory footprint and more
 * overhead. Moreover, please recall that every operating system poses hard limits on how many concurrent HTTP connections may be opened to the same end-point (i.e. host name 
 * and port). So, if you are going to use <B>more that one LSClient to the same server</B> check with the <CODE>MaxConcurrentSessionsPerServerExceededPolicy</CODE> parameter
 * on LSConnectionInfo. With it, you can specify how the LSClient should behave in case this limit is exceeded. Finally, keep in mind that the library does not know
 * <I>a priori</I> this limit, it simply confronts the number of open connections to the same server with the static parameter </CODE>MaxConcurrentSessionsPerServer</CODE>
 * of LSConnectionInfo, which is our best guess of this limit. Highering or loweing this parameter is possible although discouraged, and may lead to unexpected results.
 */
