//
//  LSConnectionStatus.h
//  Lightstreamer client for iOS
//

#ifndef Lightstreamer_client_for_iOS_LSConnectionStatus_h
#define Lightstreamer_client_for_iOS_LSConnectionStatus_h


/**
 * The LSConnectionStatus enum defines possible states of a connection.
 */
typedef enum {
	
	/**
	 * The LSClient is disconnected.
	 */
	LSConnectionStatusDisconnected= 0,
	
	/**
	 * The LSClient is connecting (or reconnecting) right now.
	 */
	LSConnectionStatusConnecting,
	
	/*
	 * The LSClient is connected in streaming mode and the connection is working regularly.
	 */
	LSConnectionStatusConnectedInStreamingMode,
	
	/*
	 * The LSClient is connected in polling mode and the connection is working regularly.
	 */
	LSConnectionStatusConnectedInPollingMode,

	/**
	 * The LSClient is connected but the connection failed to send data or keepalives for the configured timeout.
	 */
	LSConnectionStatusStalled
	
} LSConnectionStatus;


#endif
