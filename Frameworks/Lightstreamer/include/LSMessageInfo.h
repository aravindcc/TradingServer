//
//  LSMessageInfo.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>

#define NO_DELAY_TIMEOUT (-1.0)
#define UNORDERED_MESSAGES (@"UNORDERED_MESSAGES")

/**
 * The LSMessageInfo class contains a message object that will be sent to a Lightstreamer Server. 
 * The class contains the message string, the sequence to which such message is associated and a timeout
 * to wait before the server declares lost the previous message in sequence.
 */
@interface LSMessageInfo : NSObject <NSCopying>
{
}

#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSMessageInfo object with the specified parameters.
 *
 * @param message The text string to be sent to the Server. It will be interpreted by the Metadata Adapter.
 * @param sequence The sequence identifier for this Message. Messages pertaining to the same sequence are 
 * guaranteed to be processed by the server in order. If sequential management is undesired, the special UNORDERED_MESSAGES 
 * sequence identifier can be used. A sequence can be composed only of alphanumeric characters and underscores.
 *
 * @return The LSMessageInfo object.
 */
+ (LSMessageInfo *)messageInfoWithMessage:(NSString *)message sequence:(NSString *)sequence;

/**
 * Creates and returns an LSMessageInfo object with the specified parameters.
 *
 * @param message The text string to be sent to the Server. It will be interpreted by the Metadata Adapter.
 * @param sequence The sequence identifier for this Message. Messages pertaining to the same sequence are 
 * guaranteed to be processed by the server in order. If sequential management is undesired, the special UNORDERED_MESSAGES 
 * sequence identifier can be used. A sequence can be composed only of alphanumeric characters and underscores.
 * @param delayTimeout The timeout the Server should wait in case of a hole in the sequence's progressive 
 * before declaring the previous message as lost. If 0 means that the Server will not wait at all for missing messages.
 * If less than 0 the Server will use a timeout based on its own configuration.
 *
 * @return The LSMessageInfo object.
 */
+ (LSMessageInfo *)messageInfoWithMessage:(NSString *)message sequence:(NSString *)sequence delayTimeout:(NSTimeInterval)delayTimeout;

- (id)initWithMessage:(NSString *)message sequence:(NSString *)sequence delayTimeout:(NSTimeInterval)delayTimeout;

#pragma mark -
#pragma mark Properties

/**
 * The text string to sent to the Server.
 */
@property(nonatomic, copy) NSString *message;

/**
 * The sequence identifier for this Message. A sequence can be composed only of alphanumeric characters and underscores.
 */
@property(nonatomic, copy) NSString *sequence;

/**
 * The timeout the Server should wait in case of a hole in the sequence's progressive 
 * before declaring the previous message as lost. If 0 means that the Server will not wait at all for missing messages.
 * If less than 0 the Server will use a timeout based on its own configuration.
 * <BR/>The default is -1.
 */
@property(nonatomic, assign) NSTimeInterval delayTimeout;

/**
 * The progressive number assigned to this message in its sequence. This value is assigned automatically
 * when the message is sent. Any user-assigned value is ignored.
 * <BR/>The default value is 0, which means the message has not yet been sent.
 */
@property(nonatomic, assign) int progressiveNumber;

@end
