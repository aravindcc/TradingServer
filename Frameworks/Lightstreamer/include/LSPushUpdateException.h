//
//  LSPushUpdateException.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>
#import "LSException.h"

/**
 * The LSPushUpdateException class incapsulates exceptions thrown due to unexpected or wrong data updates received from the Server.
 */
@interface LSPushUpdateException : LSException
{
}

#pragma mark -
#pragma mark Initialization

/**
 * Creates and raises an LSPushUpdateException object with specified parameters.
 *
 * @param reason Reason of the exception.
 *
 * @return The LSPushUpdateException object.
 */
+ (void)raiseWithReason:(NSString *)reason, ...;

/**
 * Initializes an LSPushUpdateException object with specified parameters.
 *
 * @param reason Reason of the exception.
 *
 * @return The LSPushUpdateException object.
 */
- (id)initWithReason:(NSString *)reason, ...;

/**
 * Initializes an LSPushUpdateException object with specified parameters.
 *
 * @param reason Reason of the exception.
 * @param arguments Variable argument list of parameters.
 *
 * @return The LSPushUpdateException object.
 */
- (id)initWithReason:(NSString *)reason arguments:(va_list)arguments;

@end
