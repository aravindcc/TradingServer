//
//  LSTableInfo.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>
#import "LSMode.h"

/**
 * The LSTableInfo class contains the specification of a table to be subscribed to Lightstreamer Server.
 */
@interface LSTableInfo : NSObject <NSCopying>
{

  @protected
	NSString *_group;
	NSString *_schema;
}

#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns an LSTableInfo object with the specified parameters.
 *
 * @param group The name of the Group of items contained in this table.
 * @param mode The subscription mode for the items contained in this table.
 * @param schema The name of the Schema of fields used in this table.
 * @param dataAdapter The name of the Data Adapter (within the Adapter Set used by the current session) 
 * that supplies all the items in the Group. If it is nil, the "DEFAULT" name is used.
 * @param snapshot Whether or not the snapshot is being asked for the items contained in this table.
 *
 * @return The LSTableInfo object.
 */
+ (LSTableInfo *)tableInfoWithGroup:(NSString *)group mode:(LSMode)mode schema:(NSString *)schema dataAdapter:(NSString *)dataAdapter snapshot:(BOOL)snapshot;

#pragma mark -
#pragma mark Properties

/**
 * The name of the Group of items contained in this table.
 */
@property(nonatomic, copy) NSString *group;

/**
 * The subscription mode for the items contained in this table.
 */
@property(nonatomic, assign) LSMode mode;

/**
 * The name of the Schema of fields used in this table.
 */
@property(nonatomic, copy) NSString *schema;

/**
 * The name of the Data Adapter (within the Adapter Set used by the current session) 
 * that supplies all the items in the Group. If it is nil, the "DEFAULT" name is used.
 */
@property(nonatomic, copy) NSString *dataAdapter;

/**
 * The selector to be applied by the Server to the updates.
 */
@property(nonatomic, copy) NSString *selector;

/**
 * Whether or not the snapshot is being asked for the items contained in this table.
 * <BR/>The default is NO.
 */
@property(nonatomic, assign) BOOL snapshot;

/**
 * The position of the first item in the Group to be considered. 0 means no constraint.
 * <BR/>The default is 0.
 */
@property(nonatomic, assign) int start;

/**
 * the position of the last item in the Group to be considered. 0 means no constraint.
 * <BR/>The default is 0.
 */
@property(nonatomic, assign) int end;

/**
 * The requested length for the snapshot to be received for all the items in the table. If 0, the snapshot length will
 * be determined by the Server.
 * <BR/>NOTE: the snapshot length can be specified only for DISTINCT mode.
 * <BR/>The default is 0.
 */
@property(nonatomic, assign) int requestedDistinctSnapshotLength;

/**
 * the requested size for the Server ItemEventBuffer for all the items in the table. 0 means unlimited buffer. If less than 0, 
 * a 1 element buffer is meant if mode is MERGE and an unlimited buffer is meant if mode is DISTINCT. However, 
 * the real Server buffer size can be limited by the Metadata Adapter.
 * <BR/>NOTE: the buffer size can be specified only for MERGE and DISTINCT modes.
 * <BR/>It is ignored if unfilteredDispatch is set.
 * <BR/>The default is -1.
 */
@property(nonatomic, assign) int requestedBufferSize;

/**
 * The maximum update frequency for all the items in the table. 0.0 means unlimited frequency. It can only be used in order to lower
 * the frequency granted by the Metadata Adapter.
 * <BR/>NOTE:The update frequency can be specified only for MERGE, DISTINCT and
 * COMMAND mode; in the latter case, the frequency limit only applies to consecutive UPDATE events for the same key value.
 * <BR/>It is ignored if unfilteredDispatch is set.
 * <BR/>The default is 0.
 * <BR/><B>Edition Note:</B> a further global frequency limit is also imposed by the Server, if it is running in Presto, Allegro or 
 * Moderato edition; this specific limit also applies to RAW mode and to unfiltered dispatching.
 */
@property(nonatomic, assign) double requestedMaxFrequency;

/**
 * Events for the items in the table are dispatched in an unfiltered way. This setting is stronger than the setting
 * of the maximum update frequency and can lead to the subscription refusal by the Metadata Adapter. It is also stronger than the
 * setting for the buffer size.
 * <BR/>NOTE: the unfiltered dispatching can be specified only for MERGE, DISTINCT and COMMAND Items mode.
 * <BR/>The default is NO.
 */
@property(nonatomic, assign) BOOL unfilteredDispatch;

@end
