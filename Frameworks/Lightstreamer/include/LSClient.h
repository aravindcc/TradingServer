//
//  LSClient.h
//  Lightstreamer client for iOS
//

#import <Foundation/Foundation.h>
#import "LSConnectionStatus.h"

@class LSConnectionStateMachine;
@class LSConnectionInfo;
@class LSConnectionConstraints;
@class LSTableInfo;
@class LSExtendedTableInfo;
@class LSSubscribedTableKey;
@class LSSubscriptionConstraints;
@class LSMessageInfo;
@protocol LSConnectionDelegate;
@protocol LSTableDelegate;
@protocol LSMessageDelegate;

@class iSLClient;
@class iSLSession;
@protocol iSLListener;

@class LSThreadPool;

/**
 * The LSClient class incapsulates a single connection to Lightstreamer Server.
 */
@interface LSClient : NSObject
{
}

#pragma mark -
#pragma mark Initialization

/**
 * Creates and returns a new LSClient object.
 * <BR/>Note: use of multiple LSClient objects is discouraged. See the API docs 
 * home page, section "Using more than one LSClient", for more information.
 *
 * @return The LSClient object.
 */
+ (LSClient *)client;

/**
 * Initializes a new LSClient object.
 * <BR/>Note: use of multiple LSClient objects is discouraged. See the API docs
 * home page, section "Using more than one LSClient", for more information.
 *
 * @return The LSClient object.
 */
- (id)init;

#pragma mark -
#pragma mark Connection management

/**
 * Opens a connection to the Server with the supplied parameters. The Server will initiate a push session
 * for this client through a streaming connection. If a connection is open, it is closed first.
 * <BR/>The method is blocking: it will return only after a connection has been established
 * or the attempt has failed.
 * 
 * @param connectionInfo Contains the Server address and the connection parameters. A copy of the object is stored internally.
 * @param delegate Receives notification for connection events.
 * 
 * @throws LSPushServerException Thrown in case the server refuses the connection with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem while connecting to the Server.
 */
- (void)openConnectionWithInfo:(LSConnectionInfo *)connectionInfo delegate:(id<LSConnectionDelegate>)delegate;

/**
 * Requests new constraints to be applied on the overall data flow from the current connection.
 * The new limits override the limits requested with the connection operation or the last call to this method 
 * (a constraint not set means an unlimited constraint and may override a previous limiting constraint). 
 * They can only be used in order to restrict the constraints set by Lightstreamer Server Metadata Adapter.
 * <BR/>The method is blocking: it will return only after receiving the Server answer.
 * 
 * @param connectionConstraints Constraints to be applied
 * 
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void)changeConnectionWithConstraints:(LSConnectionConstraints *)connectionConstraints;

/**
 * Closes the connection to the Server, if one is open.
 * <BR/>The method is blocking: it will return only after the connection has been closed.
 */
- (void)closeConnection;

#pragma mark -
#pragma mark Subscriptions management

/**
 * Subscribes to a set of items, which share the same schema and other subscription parameters. All item and field names are provided.
 * This requires that a LiteralBasedProvider or equivalent Metadata Adapter is configured on the Server, in order to understand the request.
 * <BR/>The items are not collected as one table, but they are subscribed each in a different table. However, the items are subscribed alltogether
 * with a single connection to the Server. The items unsubscription can be made either in a single operation (through unsubscribeTables)
 * or through independent unsubscribe operations.
 * <BR/>Subscribed items are identified to the listener both by position and by by name. If the request fails, no subscriptions have been performed.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server answer.
 * Inside a batch, the method will return immediately but the request will be processed only on batch commit.
 *
 * @param tableInfo Contains the specification and request parameters of the items to subscribe to. A copy of the object is stored internally.
 * @param delegate Receives notification of data updates and subscription termination.
 * 
 * @return An array of handles to the subscribed tables, one for each item. The order of the handles reflects the order of the items in the item
 * set description. The handles are needed for unsubscription.
 *
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (NSArray *)subscribeItemsWithExtendedInfo:(LSExtendedTableInfo *)tableInfo delegate:(id<LSTableDelegate>)delegate;

/**
 * Subscribes to a table through the Server. The table is specified by providing item and field names. This requires that
 * a LiteralBasedProvider or equivalent Metadata Adapter is configured on the Server, in order to understand the request.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server answer.
 * Inside a batch, the method will return immediately but the request will be processed only on batch commit.
 *
 * @param tableInfo Contains the specification and request parameters of the items to subscribe to. A copy of the object is stored internally.
 * @param delegate Receives notification of data updates and subscription termination.
 * @param commandLogic If YES, enables the notification of item updates with "COMMAND logic", This requires that the items are subscribed to in COMMAND mode or
 * behave equivalently; in particular, that the special "key" and "command" fields are included in the schema.
 *
 * @return A handle to the subscribed table, to be used for unsubscription.
 *
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (LSSubscribedTableKey *)subscribeTableWithExtendedInfo:(LSExtendedTableInfo *)tableInfo delegate:(id<LSTableDelegate>)delegate useCommandLogic:(BOOL)commandLogic;

/**
 * Subscribes to a table through the Server. The table is specified by group name and schema name. Specific item and field names have to be determined by the Metadata Adapter,
 * and are identified to the delegate only by positional information, as item and field names are not known.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server answer.
 * Inside a batch, the method will return immediately but the request will be processed only on batch commit.
 *
 * @param tableInfo Contains the specification and request parameters of the items to subscribe to. A copy of the object is stored internally.
 * @param delegate Receives notification of data updates and subscription termination.
 * @param commandLogic If YES, enables the notification of item updates with "COMMAND logic", This requires that the items are subscribed to in COMMAND mode or
 * behave equivalently; in particular, that the special "key" and "command" fields are included in the schema.
 *
 * @return A handle to the subscribed table, to be used for unsubscription.
 *
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (LSSubscribedTableKey *)subscribeTable:(LSTableInfo *)tableInfo delegate:(id<LSTableDelegate>)delegate useCommandLogic:(BOOL)commandLogic;

/**
 * Requests new constraints to be applied on the data flow from the specified subscription.
 * The new limits override the limits requested with the subscription operation or the last call to this method.
 * <BR/>The server may refuse to apply certain constraints under specific conditions,
 * for example when changing the max frequency of an unfiltered subscription. In these cases an LSPushServerException will be raised. 
 * <BR/>The method is blocking: it will return only after receiving the Server answer.
 * 
 * @param tableKey Handle to a table as returned by a subscribe call.
 * @param subscriptionConstraints Constraints to be applied
 * 
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void)changeTableSubscription:(LSSubscribedTableKey *)tableKey withConstraints:(LSSubscriptionConstraints *)subscriptionConstraints;

/**
 * Unsubscribes from a table previously subscribed to the Server.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server answer.
 * Inside a batch, the method will return immediately but the request will be processed only on batch commit.
 *
 * @param tableKey Handle to a table as returned by a subscribe call.
 *
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void)unsubscribeTable:(LSSubscribedTableKey *)tableKey;

/**
 * Unsubscribes from a set of tables previously subscribed to the Server. The unsubscription requests are sent to the Server in a single connection.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server answer.
 * Inside a batch, the method will return immediately but the request will be processed only on batch commit.
 *
 * @param tableKeys Array of handles to tables as returned by one or more subscribe calls.
 *
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void)unsubscribeTables:(NSArray *)tableKeys;

#pragma mark -
#pragma mark Messages management

/**
 * Sends a message to Lightstreamer Server. The message is associated to the current session and is interpreted and managed by the Metadata Adapter related to the session.
 * <BR/>Upon subsequent calls to the method, the sequential management of the messages is guaranteed. However, any message that, for any reason, 
 * doesn't reach the Server can be discarded by the Server if this causes the subsequent message to be kept waiting for longer than a configurable timeout.  
 * A shorter timeout can be associated with the subsequent message itself. A sequence identifier must also be associated with the messages; the sequential management is 
 * restricted to all subsets of messages with the same sequence identifier associated. In case the sequential management is undesired the special 
 * UNORDERED_MESSAGES sequence identifier can be used.
 * <BR/>This method is always non-blocking: it will return immediately with a valid progressive number, but the message will be submitted
 * later. If inside a batch, the message will be submitted on batch commit (see <CODE>commitBatch</CODE>), reporting errors in the commit result 
 * and the outcome asynchronously to the delegate. If outside of a batch, reporting both errors and the outcome asynchronously to the delegate.
 *
 * @param messageInfo The message string to be interpreted by the Metadata Adapter, the sequence this message has to be associated with
 * and a delay timeout to be waited by the server for missing messages before considering them lost. A copy of the object is stored internally.
 * @param delegate Receives notification of the outcome of the message request.
 *
 * @return A number representing the progressive number of the message within its sequence is returned (starting from 1). Note that each time a new session is established
 * the progressive number is reset.
 */
- (int)sendMessage:(LSMessageInfo *)messageInfo delegate:(id<LSMessageDelegate>)delegate;

/**
 * Sends a message to Lightstreamer Server. The message is associated to the current session and is interpreted and managed by the Metadata Adapter related to the session.
 * <BR/>The method is blocking if executed outside of a batch, else it is non-blocking: outside of a batch, it will return only after receiving the Server answer.
 * Inside a batch, the method will return immediatly but the request will be processed only on batch commit.
 *
 * @param message Any text string, to be interpreted by the Metadata Adapter.
 *
 * @throws LSPushServerException Thrown in case the server refuses the request with a specific error code.
 * @throws LSPushConnectionException Thrown in case of any other problem (including if the client is not connected).
 */
- (void)sendMessage:(NSString *)message;

#pragma mark -
#pragma mark Request batching

/**
 * Signals that the next requests should be accumulated and sent to Lightstreamer Server with a single connection. The batch will continue to accomulate requests until a
 * commit is requested. When the connetion is closed, any ongoing batch that has not been committed will be automatically aborted.
 * <BR/>Note that subscription requests and message requests are batched separately: all subscriptions will be executed first, followed by messages.
 * <BR/>Note also that batches cannot be nested: there is at most one and only batch ongoing for a given LSClient object.
 *
 * @throws LSPushConnectionException Thrown if the client is not connected.
 */
- (void)beginBatch;

/**
 * Commits the current batch. Pending requests will be sent to Lightstreamer Server in the order they have been accomulated in the batch, in two separate blocks:
 * subscription/unsubscription requests will be sent first, followed by message requests. The outcome of each request will be accomulated in a list and returned:
 * if the outcome is positive an appropriate object will be added (i.e. NSNull for requests without return values, NSNumber for requests with boolean return value,
 * NSString for requests with string return values, or the appropriate object for requests with object return values), else the appropriate exception will be added. 
 * The implementation guarantees an outcome for each request.
 * <BR/>This method is blocking: it will return only after receiving the Server answer of all the requests.
 *
 * @return The list of the outcomes 
 *
 * @throws LSPushConnectionException Thrown if the client is not connected.
 */
- (NSArray *)commitBatch;

/**
 * Aborts the current batch. Pending requests will be lost: any subscription handle or progressive number obtained from a request that is part of the ongoing batch
 * will have no value.
 *
 * @throws LSPushConnectionException Thrown if the client is not connected.
 */
- (void)abortBatch;

#pragma mark -
#pragma mark Properties

/**
 * Tells if the client is connected. If NO, any operation requiring an active connection will be refused.
 */
@property(nonatomic, readonly, getter=isConnected) BOOL connected;

/**
 * Tells if the client has an ongoing batch.
 */
@property(nonatomic, readonly, getter=isBatching) BOOL batching;

/**
 * Tells the current connection status.
 */
@property(nonatomic, readonly) LSConnectionStatus connectionStatus;

/**
 * Tells the current connection delegate.
 */
@property(nonatomic, readonly) id<LSConnectionDelegate> delegate;

@end
