import Vapor
import TradingCore

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "It works" example
    router.get { req -> String in
        return "Hello World!"
    }
    
    try router.register(collection: TradeController())
}

struct TradeController: RouteCollection {
    func boot(router: Router) throws {
        let group = router.grouped("add")
        group.post(use: createHandler)
        group.get(use: infoHandler)
    }
    
    func createHandler(_ req: Request) throws -> Future<String> {
        try req.content.decode(TradeOrder.self).map({ order in
            let lg = try req.make(Logger.self)
            lg.info("Received Trade \(order.patternKey)")
            return sharedTrader.add(order: order) ? "Added Trade" : "Updated Trade"
        })
    }
    
    let infoHandler = { (req: Request) -> Future<[TradeOrder]> in
        return TradeOrder.query(on: req).all()
    }
}
