import Vapor
import TradingCore
import FluentPostgreSQL

/// Called after your application has initialized.
var sharedTrader: AutoTrade!
public func boot(_ app: Application) throws {
    // Start Trader
    sharedTrader = try AutoTrade(app: app)
}


class DBHandler: TradeDatabaseDelegate {
    var conn: PostgreSQLConnection!
    
    init(app: Application) throws {
        self.conn = try app.requestCachedConnection(to: .psql).wait()
    }
    
    func getPrevious() -> Future<[TradeOrder]> {
         let resp = TradeOrder.query(on: conn).all()
         return resp
    }
    
    func create(order: TradeOrder) -> Void {
        let res = order.create(on: conn)
        res.catch { (error) in
            print(error)
        }
    }
    
    @discardableResult
    func update(order: TradeOrder) -> Bool {
        guard let ogID = order.id else { return false }
        let _ = order.update(on: conn, originalID: ogID)
        return true
    }
    
    func delete(order: TradeOrder) -> Void {
        let _ = order.delete(on: conn)
    }
}
