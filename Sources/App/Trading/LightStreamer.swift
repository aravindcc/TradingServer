//
//  LightStreamer.swift
//  App
//
//  Created by clar3 on 17/04/2020.
//

import Foundation
import Vapor
import TradingCore


struct LSPriceResponse: Codable {
    var AuditId: String
    var Bid: Double
    var Offer: Double
}

struct LSConnectRequest: Codable {
    var url: String
    var username: String
    var password: String
    var adapter: String
}

class LightStreamer: NSObject {
    private var trader: TradingClient!
    private var lightClientHost = Environment.get("LightClientHost") ?? "localhost"
    private var lightClientPort = Int(Environment.get("LightClientPort") ?? "3000")
    private var loginToken: String!
    private var username: String!
    
    init(_ trader: TradingClient) throws {
        super.init()
        self.trader = trader
        guard let token = trader.sessionToken else { throw TradingClientError.operationFailed }
        loginToken = token
        username = trader.username
        try connect().wait()
    }
    
    func connect() throws -> Future<Void> {
        let post = LSConnectRequest(url: "https://push.cityindex.com/",
                                     username: username,
                                     password: loginToken,
                                     adapter: "STREAMINGALL")
        let body = try JSONEncoder().encode(post)
        let headers = HTTPHeaders([
            ("Content-Type", "application/json")
        ])
        let request = HTTPRequest(method: .POST, url: "/connect", headers: headers, body: body)
        let client = HTTPClient.connect(hostname: lightClientHost,
                                        port: lightClientPort,
                                        on: trader.worker)
        return client.send(request: request).map { (resp: HTTPResponse) in
            if resp.status.code != 200 {
                throw TradingClientError.connectionFail
            }
        }
    }
    
    func getAuditID(_ pair: CurrencyPair) throws -> Future<LSPriceResponse> {
        guard let id = trader.marketIDs[pair] else { throw TradingClientError.operationFailed }
        return getAuditID(id)
    }
    
    func getAuditID(_ marketID: Int) -> Future<LSPriceResponse> {
        let request = HTTPRequest(method: .GET, url: "/audit/\(marketID)/")
        let client = HTTPClient.connect(hostname: lightClientHost,
                                        port: lightClientPort,
                                        on: trader.worker)
        return client.send(request: request).map { (resp: HTTPResponse) in
            let decoder = JSONDecoder()
            guard   resp.status.code == 200,
                    let data = resp.body.data,
                    let response = try? decoder.decode(LSPriceResponse.self, from: data)
            else { throw TradingClientError.connectionFail }
            return response
        }.catch { (error) in
            print(error)
        }
    }
}
