//
//  FXTradingClient.swift
//  App
//
//  Created by clar3 on 17/04/2020.
//

import Foundation
import TradingCore
import Async
import HTTP

extension CurrencyPair {
    var baseCurrency: String {
        let split = baseText.split(separator: "/")
        return String(split[1])
    }
}

class FXTradingClient: TradingClient {
    private var ls: LightStreamer!
    
    override init(worker: Worker) throws {
        try super.init(worker: worker)
        ls = try LightStreamer(self)
    }
    public func availableFunds() -> Future<AccountMarginResponse> {
        let url = "/margin/ClientAccountMargin"
        return apiRequest(to: url, using: .GET)
    }
    
    public func place(trade: TradeOrder) throws -> Future<TradeOrderResponse> {
        return try stoplimit(order: trade)
    }
    
    public func update(trade id: Int, details trade: TradeOrder) -> Future<TradeOrderResponse?> {
        let order = getTradeDetail(of: id)
        return order.flatMap { detail in
            do {
                if detail?.TypeId == .entry {
                    return try self.stoplimit(order: trade, updateOrder: detail!)
                } else if detail?.TypeId == .trade {
                    return try self.update(trade: detail!, details: trade)
                }
                throw TradingClientError.operationFailed
            } catch {
                return self.at(once: nil)
            }
        }
    }
    
    private func update(trade already: ActiveOrderResponse, details trade: TradeOrder) throws -> Future<TradeOrderResponse?> {
        guard   let id = already.TradeOrder?.OrderId,
                let stopId = already.TradeOrder?.IfDone.first?.Stop?.OrderId,
                let limitId = already.TradeOrder?.IfDone.first?.Limit?.OrderId,
                let tp = trade.tps.last,
                let marketID = marketIDs[trade.pair],
                let size = already.TradeOrder?.Quantity
        else {
            throw TradingClientError.operationFailed
        }
        return flatMap(getMarketInformation(marketID), try ls.getAuditID(trade.pair)) { (marketInfo, currentPrice) in
            let direc: Direction = trade.bullish ? .buy : .sell
            let reverseDirec: Direction = !trade.bullish ? .buy : .sell
            
            let mmMove = marketInfo.MarketInformation.MinDistance * 1.05
            
            let sF: (Double, Double) -> Double = trade.bullish ? min : max
            let tF: (Double, Double) -> Double = trade.bullish ? max : min
            let minSL = trade.bullish ? currentPrice.Bid - mmMove : currentPrice.Bid + mmMove
            let minTP = trade.bullish ? currentPrice.Bid + mmMove : currentPrice.Bid - mmMove
            
            let minMove = 1 / (pow(10, marketInfo.MarketInformation.PriceDecimalPlaces) as NSDecimalNumber).doubleValue
            let modSl = sF(minSL, self.nearest(trade.currentSL, toNearest: minMove))
            let modTp = tF(minTP, self.nearest(tp, toNearest: minMove))
            let slEtTp = IfDoneOrderRequest(Stop: StopLimitOrder(TriggerPrice: modSl, Direction: reverseDirec.rawValue, Quantity: size, OrderId: stopId, Guaranteed: false),
                                            Limit: StopLimitOrder(TriggerPrice: modTp, Direction: reverseDirec.rawValue, Quantity: size, OrderId: limitId, Guaranteed: false))
            let params = UpdateTraderOrderRequest(OrderId: id,
                                                  MarketId: marketInfo.MarketInformation.MarketId,
                                                  MarketName: marketInfo.MarketInformation.Name,
                                                  Direction: direc.rawValue,
                                                  Quantity: size,
                                                  TradingAccountId: self.tradingAccount,
                                                  IfDone: [slEtTp],
                                                  isTrade: true,
                                                  AuditId: currentPrice.AuditId,
                                                  BidPrice: currentPrice.Bid,
                                                  OfferPrice: currentPrice.Offer,
                                                  PositionMethodId: .LongOrShortOnly)
            let url = "/order/updatetradeorder"
            return self.apiRequest(to: url, using: .POST, params: params)
        }
    }
    
    public func getTradeDetail(of tradeID: Int) -> Future<ActiveOrderResponse?> {
        let url = "/order/\(tradeID)"
        let promise = worker.eventLoop.newPromise(ActiveOrderResponse?.self)
        let resp: Future<GetOrderResponse> = apiRequest(to: url, using: .GET)
        resp.do { (active) in
            if let trade = active.TradeOrder {
                promise.succeed(result: ActiveOrderResponse(TradeOrder: trade, StopLimitOrder: nil, TypeId: .trade))
            } else if let entry = active.StopLimitOrder, entry.ParentOrderId == nil {
                promise.succeed(result: ActiveOrderResponse(TradeOrder: nil, StopLimitOrder: entry, TypeId: .entry))
            } else {
                promise.succeed(result: nil)
            }
        }.catch { (_) in
            promise.succeed(result: nil)
        }
        return promise.futureResult
    }
    
    public func getActiveTrades() -> Future<[PositionResponse]> {
        return getActivePositions()
        //return map(getActiveEntries(), getActivePositions()) { $0 + $1 }
    }
    
    public func getActiveEntries() -> Future<[PositionResponse]> {
        let url = "/order/activestoplimitorders?TradingAccountId=\(tradingAccount)"
        return apiRequest(to: url, using: .GET).map { (resp: ActiveStopLimitOrderListResponse) in
            return resp.ActiveStopLimitOrders.filter {
                $0.ParentOrderId == nil
            }
        }
    }
    
    public func getActivePositions() -> Future<[PositionResponse]> {
        let url = "/order/openpositions?TradingAccountId=\(tradingAccount)"
        return apiRequest(to: url, using: .GET).map { (resp: OpenPositionsResponse) in
            return resp.OpenPositions
        }
    }
    
    public func close(trade order: TradeOrder) -> Future<TradeOrderResponse?> {
        guard let tradeID = order.tradeID else { return at(once: nil) }
        let detail: Future<ActiveOrderResponse?> = getTradeDetail(of: tradeID)
        return detail.flatMap { (response) in
            if let trade = response?.TradeOrder, let marketID = trade.MarketId {
                return flatMap(self.getMarketInformation(marketID), self.ls.getAuditID(marketID)) { (marketInfo, currentPrice) in
                    let url = "/order/newtradeorder"
                    let direc = trade.Direction == Direction.buy ? Direction.sell : Direction.buy
                    let params = CloseTradeOrderRequest(TradingAccountId: self.tradingAccount,
                                                        OrderId: 0,
                                                        MarketId: marketInfo.MarketInformation.MarketId,
                                                        MarketName: marketInfo.MarketInformation.Name,
                                                        Direction: direc.rawValue,
                                                        Quantity: trade.Quantity,
                                                        Close: [tradeID],
                                                        isTrade: true,
                                                        AuditId: currentPrice.AuditId,
                                                        BidPrice: currentPrice.Bid,
                                                        OfferPrice: currentPrice.Offer,
                                                        PositionMethodId: .LongOrShortOnly)
                    return self.apiRequest(to: url, using: .POST, params: params)
                }
            } else if let trade = response?.StopLimitOrder, trade.ParentOrderId == nil {
                let url = "/order/cancel"
                let params = CloseEntryOrderRequest(TradingAccountId: self.tradingAccount, OrderId: trade.OrderId)
                return self.apiRequest(to: url, using: .POST, params: params)
            }
            return self.at(once: nil)
        }
    }
    
    private func getMarketInformation(_ marketID: Int) -> Future<MarketInfoResponse> {
        return apiRequest(to: "/market/\(marketID)/information", using: .GET)
    }
    
    private func stoplimit<T: Codable>(order trade: TradeOrder, updateOrder already: ActiveOrderResponse? = nil) throws -> Future<T> {
        guard let marketID = marketIDs[trade.pair], let tp = trade.tps.last else { throw TradingClientError.connectionFail }
        let id = already?.StopLimitOrder?.OrderId ?? 0
        let stopId = already?.StopLimitOrder?.IfDone.first?.Stop?.OrderId
        let limitId = already?.StopLimitOrder?.IfDone.first?.Limit?.OrderId
        let direc: Direction = trade.bullish ? .buy : .sell
        let reverseDirec: Direction = !trade.bullish ? .buy : .sell
        let entry = trade.entry
        let simulating = T.self == SimulatedTradeOrderResponse.self
        let add = simulating ? "/simulate" : ""
        let isNew = id == 0
        let url = isNew ? "/order\(add)/newstoplimitorder" : "/order\(add)/updatestoplimitorder"
        return flatMap(getMarketInformation(marketID),
                       calculatePositionSize(),
                       try ls.getAuditID(trade.pair))
        { (marketInfo, scalingSize, currentPrice) in
            let size: Double = max(scalingSize, 1000)
            let minMove = 1 / (pow(10, marketInfo.MarketInformation.PriceDecimalPlaces) as NSDecimalNumber).doubleValue
            let modEnt = self.nearest(entry, toNearest: minMove)
            let modSl = self.nearest(trade.sl, toNearest: minMove)
            let modTp = self.nearest(tp, toNearest: minMove)
            let slEtTp =
                simulating ? [] :
                    [IfDoneOrderRequest(Stop: StopLimitOrder(TriggerPrice: modSl, Direction: reverseDirec.rawValue, Quantity: size, OrderId: stopId),
                                       Limit: StopLimitOrder(TriggerPrice: modTp, Direction: reverseDirec.rawValue, Quantity: size, OrderId: limitId))]
            let params = NewTradeOrderRequest(OrderId: id,
                                              MarketId: marketID,
                                              MarketName: marketInfo.MarketInformation.Name,
                                              Direction: direc.rawValue,
                                              Quantity: size,
                                              TriggerPrice: modEnt,
                                              TradingAccountId: self.tradingAccount,
                                              PositionMethodId: .LongOrShortOnly,
                                              AuditId: currentPrice.AuditId,
                                              BidPrice: currentPrice.Bid,
                                              OfferPrice: currentPrice.Offer,
                                              isTrade: false,
                                              Reference: "GCAPI",
                                              IfDone: slEtTp)
            return self.apiRequest(to: url, using: .POST, params: params)
        }
    }
    
    private func nearest(_ value: Double, toNearest: Double) -> Double {
        return round(value / toNearest) * toNearest
    }
    
    private func calculatePositionSize() -> Future<Double> {
        return availableFunds().map { margin in
            return self.nearest(self.nearest(margin.Cash, toNearest: 250) * (5000 / 1500), toNearest: 10)
        }
    }
}
