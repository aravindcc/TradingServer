//
//  TradingClient.swift
//  harmonic
//
//  Created by clar3 on 02/04/2020.
//  Copyright © 2020 Ashla. All rights reserved.
//

import Foundation
import TradingCore
import Vapor

extension TradeOrder {
    func update(from order: TradeOrder) {
        if !enteredPosition {
            entry = order.entry
            sl = bullish ? min(sl, order.sl) : max(sl, order.sl)
        } else {
            sl = order.sl
        }
        tps = order.tps
        patternEntries = order.patternEntries
    }
    
    var jsonRep: String {
        if  let data = try? JSONEncoder().encode(self),
            let str = String(data: data, encoding: .utf8) {
            return str
        }
        return patternKey
    }
}

public protocol TradeDatabaseDelegate {
    func getPrevious() -> Future<[TradeOrder]>
    func create(order: TradeOrder) -> Void
    func delete(order: TradeOrder) -> Void
    
    @discardableResult
    func update(order: TradeOrder) -> Bool
}

public class AutoTrade: NSObject {
    
    public var db: TradeDatabaseDelegate!
    var trader: FXTradingClient!
    private var logger: Logger!
    
    init(app: Application) throws {
        super.init()
        logger = try app.make(Logger.self)
        trader = try FXTradingClient()
        logger.info("Connected to Trading API")
        db = try DBHandler(app: app)
        let orders = (try? db.getPrevious().wait()) ?? []
        try start(with: orders).wait()
        logger.info("Loaded Previous Orders")
        trader.getOutputLength = { length in
            return 3
        }
        if (queue.count > 0) {
            logger.info("Remonitoring: \(queue.map { $0.0.patternKey })")
            next()
        }
    }
    
    private func start(with orders: [TradeOrder]) -> Future<Void> {
        var ids : [TradeOrder] = []
        for order in orders {
            if order.tradeID == nil {
                let _ = db?.delete(order: order)
            } else {
                ids.append(order)
            }
        }
        let check = { (id: Int) -> TradeOrder? in
            for trade in ids {
                if id == trade.tradeID {
                    return trade
                }
            }
            return nil
        }
        return trader.getActiveTrades().map { trades in
            var activeOrders = [TradeOrder]()
            for active in trades {
                if let order = check(active.OrderId) {
                    activeOrders.append(order)
                }
            }
            for id in ids {
                var found = !id.patternSettled
                if !found {
                    for active in activeOrders {
                       if active == id { found = true; break }
                    }
                }
                if !found {
                    let _ = self.db?.delete(order: id)
                } else {
                    self.queue.append((id, Date()))
                }
            }
        }
    }
    
    // MARK: ORDER MANAGEMENT
    private func logTrading(message: String, error: Bool = false) -> Void {
        if error {
            self.logger.error(message)
        } else {
            self.logger.info(message)
        }
    }
    
    // place order with stop loss
    private func attach(id: Int, to order: TradeOrder, completion: ((TradeOrder) -> Void)? = nil) -> Void {
        orderProcessQueue.async {
            order.tradeID = id
            completion?(order)
        }
    }
    
    private func makeTradingAction(operation: String,
                                   id: String,
                                   action: @escaping () throws -> Future<TradeOrderResponse?>,
                                   completion: ((TradeOrderResponse?) -> Void)? = nil) -> Void {
        DispatchQueue.global(qos: .background).async {
            let logError = { (error: String) in
                self.logTrading(message: "\(operation) on \(id) error: \(error)", error: true)
            }
            let logSuccess = { (obj: TradeOrderResponse) in
                self.logTrading(message: "\(operation) on \(id) success: \(obj)")
                completion?(obj)
            }
            do {
                let req = try action()
                req.do { (response) in
                    if response != nil && response!.Status != -1 {
                        logSuccess(response!)
                    } else {
                        logError(response?.ErrorMessage ?? "ERROR")
                    }
                } .catch { error in
                    logError(error.localizedDescription)
                }
            } catch {
                logError(error.localizedDescription)
            }
        }
    }
    
    // place an order
    func place(order: TradeOrder, completion: ((TradeOrder) -> Void)? = nil) -> Void {
        makeTradingAction(operation: "Place", id: order.jsonRep, action: { () -> EventLoopFuture<TradeOrderResponse?> in
            return try self.trader.place(trade: order).map { $0 }
        }) { resp in
            if let resp = resp {
                self.attach(id: resp.OrderId, to: order, completion: completion)
            }
        }
    }
    
    
    // update order's stop loss, tp and potentially entry!
    func update(order: TradeOrder) -> Void {
        guard let orderID = order.tradeID else { return }
        makeTradingAction(operation: "Update Live", id: order.jsonRep, action: { () -> EventLoopFuture<TradeOrderResponse?> in
            return self.trader.update(trade: orderID, details: order)
        }) { resp in
            if let resp = resp {
                self.attach(id: resp.OrderId, to: order)
            }
        }
    }
    
    // close order
    func close(order: TradeOrder) -> Void {
        makeTradingAction(operation: "Close", id: order.jsonRep, action: { () -> EventLoopFuture<TradeOrderResponse?> in
            return self.trader.close(trade: order).map { resp in
                guard let resp = resp else { throw TradingClientError.operationFailed }
                return resp
            }
        })
    }
    
    // MARK: Pattern Submission
    // add pattern -> result?
    private var queue: [(TradeOrder, Date)] = []
    private let orderProcessQueue = DispatchQueue(label: "orderProcessQueue")
    private let refreshTime: Double = 60
    private var kickedOff = false
    
    @discardableResult
    func add(order: TradeOrder) -> Bool {
        orderProcessQueue.sync {
            for (i, (ord, _)) in queue.enumerated() {
                if order == ord {
                    // update p
                    let og = queue[i].0
                    og.update(from: ord)
                    if og.patternSettled {
                        update(order: og)
                    }
                    db?.update(order: ord)
                    return false
                }
            }
            // add to queue: make sure pattern has settled
            queue.append(((order, Date(timeIntervalSinceNow: refreshTime))))
            logger.info("CURRENT QUEUE: \(queue.map { $0.0.patternKey })")
            db?.create(order: order)
            
            if !kickedOff {
                kickedOff = true
                DispatchQueue.global(qos: .background).async {
                    self.next()
                }
            }
            return true
        }
    }
    func pullFromQueue() -> (TradeOrder)? {
        orderProcessQueue.sync {
            for i in 0..<queue.count {
                let (order, date) = queue[i]
                let diff =  date.timeIntervalSinceNow
                if diff <= 0 {
                    queue[i] = (order, Date(timeIntervalSinceNow: refreshTime))
                    return (order)
                }
            }
            return nil
        }
    }
    func next() {
        if let order = pullFromQueue() {
            self.monitor(order)
            self.next()
        } else {
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + (refreshTime / 4)) {
                self.next()
            }
        }
    }
    func remove(order: TradeOrder) {
        for (i, (ord, _)) in queue.enumerated() {
            if ord == order {
                queue.remove(at: i)
                break
            }
        }
        self.close(order: order)
        self.db?.delete(order: order)
    }
    
    // MARK: ORDER MONITORING
    // get 1min data of price for instrument in queue fashion as before
    func monitor(_ order: TradeOrder) {
        if order.patternSettled {
            // get price and call func below
            trader.getExchangeRate(of: order.pair, type: order.bullish ? .bid : .ask, completion: { (rate) in
                self.orderProcessQueue.sync {
                    self.monitor(order, currentPrice: rate)
                }
            }, onError: { (error) in
                print("FAILED TO GET EXCHANGE RATE \(error)")
                // don't worry hopefully will work next time
            })
        } else {
            let chosenInstrument = order.pair
            let chosenFrame = order.frame
            let outputType: OutputType = (order.frame == .daily || order.frame == .weekly) ? .compact : .full
            trader.getDataFor(pair: chosenInstrument, at: chosenFrame, forSize: outputType, forType: .mid, { (result) in
                guard   let result = result,
                        let (_, source) = result.first,
                        let lastDate = source.last?.date
                else { return }
                if order.startDate != lastDate {
                    self.orderProcessQueue.sync {
                        // so place order!
                        self.place(order: order)
                        self.logger.info("PLACING TRADE: \(order.jsonRep)")
                        order.patternSettled = true
                        order.quantumFrom = 1
                        self.db?.update(order: order)
                        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1) {
                            self.monitor(order)
                        }
                    }
                }
            })
        }
    }
    func monitor(_ order: TradeOrder, currentPrice: ExchangeRate) {
        
        let ishQuantum = Int(floor((Date().timeIntervalSince1970 - order.startDate.timeIntervalSince1970) / order.frame.secondValue))
        if let waitingTakeTp = order.waitingTakeTp, ishQuantum != order.quantumFrom {
            order.quantumFrom = ishQuantum
            if order.laggingTakeTp == 2 {
                order.laggingTakeTp = 0
                order.currentSL = Strategy.trailingStopLoss(currentAt: order.currentAt == nil ? order.entry : order.currentAt!, taketp: waitingTakeTp, currentSL: order.currentSL)
                order.currentAt = waitingTakeTp
                //order.waitingTakeTp = nil
                logTrading(message: "YO YO STOP LOSS HIT \(order.jsonRep)")
                self.update(order: order)
                self.db?.update(order: order)
            }
            order.laggingTakeTp += 1
        }
        
        let bullish = order.bullish
        let compare = { (price: Double, limit: Double) in
            return bullish ? price >= limit : price <= limit
        }
        // NOT REALY ASK AND BID THINK OF ASK HIGH AND LOW INSTEAD
        let cp = bullish ? currentPrice.ask : currentPrice.bid // usually
        let scp = !bullish ? currentPrice.ask : currentPrice.bid // usually
        if !order.enteredPosition {
            if let taketp = order.tps.first, compare(cp, taketp) {
                self.remove(order: order)
                logTrading(message: "TP PREMATURELY HIT REMOVING \(order.jsonRep)")
            } else if currentPrice.ask > order.entry {
                order.enteredPosition = true
                logTrading(message: "ENTERING POSITION \(order.jsonRep)")
                // This should be taken care of by the entry order!
            }
        } else {
            if !compare(scp, order.currentSL) {
                // reached SL
                // This should be taken care of by the entry order!
                logTrading(message: "STOP LOSS HIT \(order.jsonRep)")
                self.remove(order: order)
                return
            }
            if let taketp = order.tps.first, compare(cp, taketp) {
                if order.tps.count == 1 {
                    // reached final tp
                    // This should be taken care of by the entry order!
                    logTrading(message: "FINAL TP HIT \(order.jsonRep)")
                    self.remove(order: order)
                    return
                }
                if order.currentAt == nil {
                    order.waitingTakeTp = order.tps.removeFirst()
                    logTrading(message: "FIRST TP HIT \(order.jsonRep)")
                }
                else {
                    let _ = order.tps.removeFirst()
                    let bef = order.currentAt == nil ? order.entry : order.currentAt!
                    order.currentSL = Strategy.trailingStopLoss(currentAt: bef, taketp: taketp, currentSL: order.currentSL)
                    order.currentAt = taketp
                    logTrading(message: "NEXT TP HIT \(order.jsonRep)")
                }
                
                self.update(order: order)
                self.db?.update(order: order)
            }
        }
    }
}
