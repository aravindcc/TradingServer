//
//  FXTradingStructs.swift
//  App
//
//  Created by clar3 on 17/04/2020.
//

import Foundation

public struct AccountMarginResponse: Codable {
    public var Cash: Double
    public var Margin: Double
    public var MarginIndicator: Double
    public var NetEquity: Double
    public var OpenTradeEquity: Double
    public var TradableFunds: Double
    public var PendingFunds: Double
    public var TradingResource: Double
    public var TotalMarginRequirement: Double
    public var CurrencyId: Int
    public var CurrencyIsoCode: String
}

public enum Direction: String, Codable {
    case buy = "Buy"
    case sell = "Sell"
}

public func ==(lhs: String, rhs: Direction) -> Bool {
    return lhs.lowercased() == rhs.rawValue.lowercased() 
}

public struct StopLimitOrder: Codable {
    public var TriggerPrice: Double
    public var Direction: String?
    public var Quantity: Double?
    public var OrderId: Int = 0
    public var Applicability: String = "GTC"
    public var Guaranteed: Bool? = nil
    public var ExpiryDateTimeUTC: String = "null"
    
    public init(TriggerPrice: Double, Direction: String? = nil, Quantity: Double? = nil, OrderId: Int? = nil, Guaranteed: Bool? = nil) {
        self.TriggerPrice = TriggerPrice
        self.Direction = Direction
        self.OrderId = OrderId ?? 0
        self.Quantity = Quantity
        self.Guaranteed = Guaranteed
    }
}

public struct IfDoneOrderRequest: Codable {
    public var Stop: StopLimitOrder?
    public var Limit: StopLimitOrder?
}

public enum PositionMethod: Int, Codable {
    case LongOrShortOnly = 1
    case LongAndShort = 2
}

public struct NewTradeOrderRequest: Codable {
    public var OrderId: Int
    public var MarketId: Int
    public var MarketName: String
    public var Direction: String
    public var Quantity: Double
    public var TriggerPrice: Double
    public var TradingAccountId: Int
    public var IfDone: [IfDoneOrderRequest]
    public var PositionMethodId: PositionMethod
    public var AuditId: String
    public var BidPrice: Double?
    public var OfferPrice: Double?
    public var isTrade: Bool
    public var Reference: String
    public var Applicability: String = "GTC"
    public var LastChangedDateTimeUTC: String = "null"
    public var LastChangedDateTimeUTCDate: String = "null"
    public var AutoRollover: Bool = false
    public var ExpiryDateTimeUTCDate: String = "null"
    public var ExpiryDateTimeUTC: String = "null"
    public var Status: String = "null"
    public var OcoOrder: String = "null"
    public var QuoteId: String = "null"
    public var orderType: String = "null"
    public var Currency: String = "null"
    
    private enum CodingKeys: String, CodingKey {
        case OrderId, MarketId, MarketName, Direction, Quantity, TriggerPrice, TradingAccountId, PositionMethodId
        case AuditId, BidPrice, OfferPrice, isTrade, Reference, IfDone, Applicability, LastChangedDateTimeUTC
        case LastChangedDateTimeUTCDate, AutoRollover, ExpiryDateTimeUTCDate, ExpiryDateTimeUTC, Status, OcoOrder, QuoteId
        case orderType = "Type"
    }

    init(OrderId: Int,
         MarketId: Int,
         MarketName: String,
         Direction: String,
         Quantity: Double,
         TriggerPrice: Double,
         TradingAccountId: Int,
         PositionMethodId: PositionMethod,
         AuditId: String,
         BidPrice: Double?,
         OfferPrice: Double?,
         isTrade: Bool,
         Reference: String,
         IfDone: [IfDoneOrderRequest] = []) {
        self.OrderId = OrderId
        self.MarketId = MarketId
        self.MarketName = MarketName
        self.Direction = Direction
        self.Quantity = Quantity
        self.TriggerPrice = TriggerPrice
        self.TradingAccountId = TradingAccountId
        self.IfDone = IfDone
        self.PositionMethodId = PositionMethodId
        self.AuditId = AuditId
        self.BidPrice = BidPrice
        self.OfferPrice = OfferPrice
        self.isTrade = isTrade
        self.Reference = Reference
    }
    
}

public struct CloseTradeOrderRequest: Codable {
    init(TradingAccountId: Int, OrderId: Int, MarketId: Int, MarketName: String, Direction: String, Quantity: Double,
         Close: [Int], isTrade: Bool, AuditId: String, BidPrice: Double?, OfferPrice: Double?, PositionMethodId: PositionMethod) {
        self.TradingAccountId = TradingAccountId
        self.OrderId = OrderId
        self.MarketId = MarketId
        self.MarketName = MarketName
        self.Direction = Direction
        self.Quantity = Quantity
        self.Close = Close
        self.isTrade = isTrade
        self.AuditId = AuditId
        self.BidPrice = BidPrice
        self.OfferPrice = OfferPrice
        self.PositionMethodId = PositionMethodId
    }
    
    public var TradingAccountId: Int
    public var OrderId: Int
    public var MarketId: Int
    public var MarketName: String
    public var Direction: String
    public var Quantity: Double
    public var Close: [Int]
    public var isTrade: Bool
    public var AuditId: String
    public var BidPrice: Double?
    public var OfferPrice: Double?
    public var PositionMethodId: PositionMethod
    public var IfDone: [IfDoneOrderRequest] = []
    public var TriggerPrice: String = "null"
    public var Applicability: String = "GTC"
    public var LastChangedDateTimeUTC: String = "null"
    public var LastChangedDateTimeUTCDate: String = "null"
    public var AutoRollover: Bool = false
    public var ExpiryDateTimeUTCDate: String = "null"
    public var ExpiryDateTimeUTC: String = "null"
    public var Status: String = "null"
    public var OcoOrder: String = "null"
    public var QuoteId: String = "null"
    public var orderType: String = "null"
    
    private enum CodingKeys: String, CodingKey {
        case OrderId, MarketId, MarketName, Direction, Quantity, TriggerPrice, TradingAccountId, PositionMethodId
        case AuditId, BidPrice, OfferPrice, isTrade, IfDone, Applicability, LastChangedDateTimeUTC, Close
        case LastChangedDateTimeUTCDate, AutoRollover, ExpiryDateTimeUTCDate, ExpiryDateTimeUTC, Status, OcoOrder, QuoteId
        case orderType = "Type"
    }
}

public struct CloseEntryOrderRequest: Codable {
    public var TradingAccountId: Int
    public var OrderId: Int
}

public struct UpdateTraderOrderRequest: Codable {
    public var OrderId: Int
    public var MarketId: Int
    public var MarketName: String
    public var Direction: String
    public var Quantity: Double
    public var TradingAccountId: Int
    public var IfDone: [IfDoneOrderRequest]
    public var isTrade: Bool
    public var AuditId: String
    public var BidPrice: Double?
    public var OfferPrice: Double?
    public var PositionMethodId: PositionMethod
    public var TriggerPrice: String = "null"
    public var Applicability: String = "GTC"
    public var LastChangedDateTimeUTC: String = "null"
    public var LastChangedDateTimeUTCDate: String = "null"
    public var AutoRollover: Bool = false
    public var ExpiryDateTimeUTCDate: String = "null"
    public var ExpiryDateTimeUTC: String = "null"
    public var Status: String = "null"
    public var OcoOrder: String = "null"
    public var QuoteId: String = "null"
    public var orderType: String = "null"
    
    private enum CodingKeys: String, CodingKey {
        case OrderId, MarketId, MarketName, Direction, Quantity, TriggerPrice, TradingAccountId, PositionMethodId
        case AuditId, BidPrice, OfferPrice, isTrade, IfDone, Applicability, LastChangedDateTimeUTC
        case LastChangedDateTimeUTCDate, AutoRollover, ExpiryDateTimeUTCDate, ExpiryDateTimeUTC, Status, OcoOrder, QuoteId
        case orderType = "Type"
    }
    
    init(OrderId: Int,
         MarketId: Int,
         MarketName: String,
         Direction: String,
         Quantity: Double,
         TradingAccountId: Int,
         IfDone: [IfDoneOrderRequest],
         isTrade: Bool,
         AuditId: String,
         BidPrice: Double? = nil,
         OfferPrice: Double? = nil,
         PositionMethodId: PositionMethod) {
        self.MarketId = MarketId
        self.MarketName = MarketName
        self.Direction = Direction
        self.Quantity = Quantity
        self.OrderId = OrderId
        self.TradingAccountId = TradingAccountId
        self.IfDone = IfDone
        self.isTrade = isTrade
        self.AuditId = AuditId
        self.BidPrice = BidPrice
        self.OfferPrice = OfferPrice
        self.PositionMethodId = PositionMethodId
    }
}

public struct TradeOrderResponse: Codable {
    public var Status: Int
    public var StatusReason: Int
    public var OrderId: Int
    public var ErrorMessage: String?
    public var Orders: [OrderResponse]?
}

public struct StopLimitOrderResponse: Codable {
    public var OrderId: Int
    public var Quantity: Double
    public var TriggerPrice: Double
}

public struct IfDoneOrderResponse: Codable {
    public var Stop: StopLimitOrderResponse?
    public var Limit: StopLimitOrderResponse?
}

public struct OrderResponse: Codable {
    public var OrderId: Int
    public var Status: Int
    public var StatusReason: Int
    public var OrderTypeId: Int
    public var Price: Double
    public var Quantity: Double
    public var TriggerPrice: Double
    public var CommissionCharge: Double
    public var IfDone: [IfDoneOrderResponse]
}

public struct SimulatedOrderResponse: Codable {
    public var Status: Int
    public var StatusReason: Int
}

public struct SimulatedTradeOrderResponse: Codable {
    public var Status: Int
    public var StatusReason: Int
    public var SimulatedCash: Double
    public var ActualCash: Double
    public var SimulatedTotalMarginRequirement: Double
    public var ActualTotalMarginRequirement: Double
    public var Orders: [SimulatedOrderResponse]
}

public enum OrderTypes: Int, Codable {
    case trade = 1
    case stop = 2
    case limit = 3
    case entry = 4
}

public struct ActiveTradeOrder: Codable {
    public var OrderId: Int
    public var Quantity: Double
    public var OriginalQuantity: Double
    public var Price: Double?
    public var OriginalPrice: Double?
    public var TradingAccountId: Int
    public var Direction: String
    public var IfDone: [IfDoneOrderResponse]
    public var MarketId: Int?
}

public struct ActiveStopLimitOrder: Codable {
    public var TriggerPrice: Double
    public var ParentOrderId: Int?
    public var OrderId: Int
    public var Quantity: Double
    public var OriginalQuantity: Double
    public var Price: Double?
    public var OriginalPrice: Double?
    public var Direction: String
    public var IfDone: [IfDoneOrderResponse]
}

public struct ActiveOrderResponse: Codable {
    public var TradeOrder: ActiveTradeOrder?
    public var StopLimitOrder: ActiveStopLimitOrder?
    public var TypeId: OrderTypes
}

public struct BasicStopLimitOrder: Codable {
    public var TriggerPrice: Double
    public var OrderId: Int
    public var Quantity: Double
}

public struct PositionResponse: Codable {
    public var OrderId: Int
    public var ParentOrderId: Int?
    public var Quantity: Double
    public var StopOrder: BasicStopLimitOrder?
    public var LimitOrder: BasicStopLimitOrder?
}

public struct ActiveStopLimitOrderListResponse: Codable {
    public var ActiveStopLimitOrders: [PositionResponse]
}

public struct ActiveOrderListRequest: Codable {
    public var TradingAccountId: Int
    public var MaxResults: Int
}

public struct OpenPositionsResponse: Codable {
    public var OpenPositions: [PositionResponse]
}

public struct GetOrderResponse: Codable {
    public var TradeOrder: ActiveTradeOrder?
    public var StopLimitOrder: ActiveStopLimitOrder?
}

public struct MarketInfoResponse: Codable {
    public var MarketInformation: MarketInfo
}

public struct MarketInfo: Codable {
    public var MarketId: Int
    public var Name: String
    public var MarginFactor: Double?
    public var MarginFactorUnits: Int
    public var MinDistance: Double
    public var MinDistanceUnits: Int
    public var WebMinSize: Double
    public var MaxSize: Double
    public var PriceDecimalPlaces: Int
    public var DefaultQuoteLength: Int
    public var ConvertPriceToPipsMultiplier: Int
}
