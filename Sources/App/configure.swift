import FluentPostgreSQL
import Vapor
import TradingCore
import SwiftyBeaverProvider

extension CurrencyPair: PostgreSQLRawEnum {}
extension TimeFrame: PostgreSQLRawEnum {}
extension TradeOrder: PostgreSQLModel {}
extension TradeOrder: Content {}
extension TradeOrder: Migration, PostgreSQLMigration {}


/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    
    if Environment.get("FX_DOCKER") == nil {
        print("RUNNING LOCALLY")
        let serverConf = NIOServerConfig.default(hostname: "localhost", port: 8080)
        services.register(serverConf)
    }
    
    // Register providers first
    try services.register(FluentPostgreSQLProvider())
    
    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)
    
    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)
    
    // configure postgres
    let dbHost = Environment.get("DATABASE_HOSTNAME") ?? "localhost"
    let dbPort = Int(Environment.get("DATABASE_PORT") ?? "5432") ?? 5432
    let dbUser = Environment.get("DATABASE_PASSWORD") ?? "vapor"
    let dbName = Environment.get("DATABASE_DB") ?? "vapor"
    let dbPass = Environment.get("DATABASE_PASSWORD") ?? "vapor"
    
    print(dbHost)
    
    var databases = DatabasesConfig()
    let databaseConfig = PostgreSQLDatabaseConfig(hostname: dbHost,
                                                  port:  dbPort,
                                                  username: dbUser,
                                                  database: dbName,
                                                  password: dbPass)
        
    
    let database = PostgreSQLDatabase(config: databaseConfig)
    databases.add(database: database, as: .psql)
    services.register(databases)
    
    // Configure migrations
    var migrations = MigrationConfig()
    migrations.add(model: TradeOrder.self, database: .psql)
    services.register(migrations)
    
    // Setup your destinations
    let console = ConsoleDestination()
    console.minLevel = .info // update properties according to your needs

    let fileDestination = FileDestination()
    let directory = DirectoryConfig.detect()
    fileDestination.logFileURL = URL(fileURLWithPath: directory.workDir)
        .appendingPathComponent("Public", isDirectory: true)
        .appendingPathComponent("server.log", isDirectory: false)
    fileDestination.asynchronously = true

    // Register the logger
    services.register(SwiftyBeaverLogger(destinations: [console, fileDestination]), as: Logger.self)

    // Optional
    config.prefer(SwiftyBeaverLogger.self, for: Logger.self)
}
